FROM swift

MAINTAINER André Roque Matheus <amatheus@mac.com>

VOLUME /app

WORKDIR /app

CMD swift test